# Sample of a custom slave image for Jenkins at CERN

This is a sample project of a customized slave image for Jenkins.
See http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html for more information.

## How to use

* Fork this project
* Edit the Dockerfile (look for TODO lines) and the GitLab-CI build to install the packages
required for your Jenkins jobs
* Follow [the instructions in the CERN Jenkins documentation](http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html)
to add this custom image to your Jenkins instance.
